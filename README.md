# Wireshark Mystic Light Dissector
Wireshark dissector for decoding MSI Mystic Light messages on USB

## Installation
### Windows
*  If not existing create the folder plugins in "%APP_DATA%\Roaming\Wireshark".
*  Copy the file MysticLight.lua to the directory "%APP_DATA%\Roaming\Wireshark\plugins".
  
### Linux
* Copy the file MysticLight.lua to the directory "/usr/lib/x86_64-linux-gnu/wireshark/plugins".

## Usage
When you followed the installation above the frames containing Mystic Light data show the protocol name USB_MYSTIC_LIGHT in the top window:
```
  7   18.013818 host    1.4.0    USB_MYSTIC_LIGHT  198   URB_CONTROL out
  ```

Opening the Setup Data section in the window below shows the decoded Mystic Light frame e.g.
```

  Setup Data
    bmRequestType: 0x21
    USB Mystic Light 162
        requestType  : 82
        jrgb1        : 02 ff 00 00 29 00 ff 00 83 00
        jrainbow1    : 02 ff 00 00 29 00 ff 00 83 00
        jcorsair     : 02 ff 00 00 29 00 ff 00 83 14
        jCorsairOuter: 02 ff 00 00 29 00 ff 00 80 14
        onboard0     : 0f ff 00 00 a9 00 ff 00 81 00
        onboard1     : 01 ff 00 00 28 00 ff 00 83 14
        onboard2     : 01 ff 00 00 28 00 ff 00 83 14
        onboard3     : 01 ff 00 00 28 00 ff 00 83 14
        onboard4     : 01 ff 00 00 28 00 ff 00 83 14
        onboard5     : 01 ff 00 00 28 00 ff 00 83 14
        onboard6     : 01 ff 00 00 28 00 ff 00 83 14
        onboard7     : 02 ff 00 00 29 00 ff 00 83 00
        onboard8     : 01 ff 00 00 28 00 ff 00 83 14
        onboard9     : 01 ff 00 00 28 00 ff 00 83 14
        onboard10    : 01 ff 00 00 28 00 ff 00 83 14
        jrgb2        : 02 ff 00 00 29 00 ff 00 83 00
        save         : 0
    Data Fragment: 5202ff00002900ff00830002ff00002900ff00830002ff00002900ff00831402ff000029…
```

If you only want to see the Mystic Light data frames you can enter usb_mystic_light in the display filter. 
