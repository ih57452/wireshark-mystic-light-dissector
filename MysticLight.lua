usb_ml_protocol = Proto("USB_mystic_light",  "USB Mystic Light protocol")

local fields = {
    requestType 	= ProtoField.uint8("usb_mystic_light.requestType",   "requestType  ", base.DEC),
    jrgb1	    	= ProtoField.bytes("usb_mystic_light.jrgb1",         "jrgb1        ", base.SPACE),
    jpipe1	    	= ProtoField.bytes("usb_mystic_light.jpipe1",        "jpipe1       ", base.SPACE),
    jpipe2	    	= ProtoField.bytes("usb_mystic_light.jpipe2",        "jpipe2       ", base.SPACE),
    jrainbow1    	= ProtoField.bytes("usb_mystic_light.jrainbow1",     "jrainbow1    ", base.SPACE),
    jrainbow2    	= ProtoField.bytes("usb_mystic_light.jrainbow2",     "jrainbow2    ", base.SPACE),
    jcorsair	    = ProtoField.bytes("usb_mystic_light.jcorsair",      "jcorsair     ", base.SPACE),
    jcorsairOuter   = ProtoField.bytes("usb_mystic_light.jcorsairOuter", "jCorsairOuter", base.SPACE),
    onboard0		= ProtoField.bytes("usb_mystic_light.onboard0",      "onboard0     ", base.SPACE),
    onboard1	    = ProtoField.bytes("usb_mystic_light.onboard1",      "onboard1     ", base.SPACE),
    onboard2	    = ProtoField.bytes("usb_mystic_light.onboard2",      "onboard2     ", base.SPACE),
    onboard3	    = ProtoField.bytes("usb_mystic_light.onboard3",      "onboard3     ", base.SPACE),
    onboard4	    = ProtoField.bytes("usb_mystic_light.onboard4",      "onboard4     ", base.SPACE),
    onboard5	    = ProtoField.bytes("usb_mystic_light.onboard5",      "onboard5     ", base.SPACE),
    onboard6	    = ProtoField.bytes("usb_mystic_light.onboard6",      "onboard6     ", base.SPACE),
    onboard7	    = ProtoField.bytes("usb_mystic_light.onboard7",      "onboard7     ", base.SPACE),
    onboard8	    = ProtoField.bytes("usb_mystic_light.onboard8",      "onboard8     ", base.SPACE),
    onboard9	    = ProtoField.bytes("usb_mystic_light.onboard9",      "onboard9     ", base.SPACE),
    onboard10       = ProtoField.bytes("usb_mystic_light.onboard10",     "onboard10    ", base.SPACE),
    jrgb2           = ProtoField.bytes("usb_mystic_light.jrgb2",         "jrgb2        ", base.SPACE),
    save		    = ProtoField.uint8("usb_mystic_light.save",          "save         ", base.DEC),
    header	    	= ProtoField.bytes("usb_mystic_light.header",        "header    ", base.SPACE),
    led0_4	    	= ProtoField.bytes("usb_mystic_light.led0_4",        "led0_4    ", base.SPACE),
    led5_9	    	= ProtoField.bytes("usb_mystic_light.led5_9",        "led5_9    ", base.SPACE),
    led10_14	    = ProtoField.bytes("usb_mystic_light.led10_14",      "led10_14  ", base.SPACE),
    led15_19	    = ProtoField.bytes("usb_mystic_light.led15_19",      "led15_19  ", base.SPACE),
    led20_24	    = ProtoField.bytes("usb_mystic_light.led20_24",      "led20_24  ", base.SPACE),
    led25_29	    = ProtoField.bytes("usb_mystic_light.led25_29",      "led25_29  ", base.SPACE),
    led30_34	    = ProtoField.bytes("usb_mystic_light.led30_34",      "led30_34  ", base.SPACE),
    led35_39	    = ProtoField.bytes("usb_mystic_light.led35_39",      "led35_39  ", base.SPACE),
    led40_44	    = ProtoField.bytes("usb_mystic_light.led40_44",      "led40_44  ", base.SPACE),
    led45_49	    = ProtoField.bytes("usb_mystic_light.led45_49",      "led45_49  ", base.SPACE),
    led50_54	    = ProtoField.bytes("usb_mystic_light.led50_54",      "led50_54  ", base.SPACE),
    led55_59	    = ProtoField.bytes("usb_mystic_light.led55_59",      "led55_59  ", base.SPACE),
    led60_64	    = ProtoField.bytes("usb_mystic_light.led60_64",      "led60_64  ", base.SPACE),
    led65_69	    = ProtoField.bytes("usb_mystic_light.led65_69",      "led65_69  ", base.SPACE),
    led70_74	    = ProtoField.bytes("usb_mystic_light.led70_74",      "led70_74  ", base.SPACE),
    led75_79	    = ProtoField.bytes("usb_mystic_light.led75_79",      "led75_79  ", base.SPACE),
    led80_84	    = ProtoField.bytes("usb_mystic_light.led80_84",      "led80_84  ", base.SPACE),
    led85_89	    = ProtoField.bytes("usb_mystic_light.led85_89",      "led85_89  ", base.SPACE),
    led90_94	    = ProtoField.bytes("usb_mystic_light.led90_94",      "led90_94  ", base.SPACE),
    led95_99	    = ProtoField.bytes("usb_mystic_light.led95_99",      "led95_99  ", base.SPACE),
    led100_104   	= ProtoField.bytes("usb_mystic_light.led100_104",    "led100_104", base.SPACE),
    led105_109   	= ProtoField.bytes("usb_mystic_light.led105_109",    "led105_109", base.SPACE),
    led110_114   	= ProtoField.bytes("usb_mystic_light.led110_114",    "led110_114", base.SPACE),
    led115_119   	= ProtoField.bytes("usb_mystic_light.led115_119",    "led115_119", base.SPACE),
    led120_124   	= ProtoField.bytes("usb_mystic_light.led120_124",    "led120_124", base.SPACE),
    led125_129   	= ProtoField.bytes("usb_mystic_light.led125_129",    "led125_129", base.SPACE),
    led130_134	    = ProtoField.bytes("usb_mystic_light.led130_134",    "led130_134", base.SPACE),
    led135_139	    = ProtoField.bytes("usb_mystic_light.led135_139",    "led135_139", base.SPACE),
    led140_144	    = ProtoField.bytes("usb_mystic_light.led140_144",    "led140_144", base.SPACE),
    led145_149	    = ProtoField.bytes("usb_mystic_light.led145_149",    "led145_149", base.SPACE),
    led150_154	    = ProtoField.bytes("usb_mystic_light.led150_154",    "led150_154", base.SPACE),
    led155_159   	= ProtoField.bytes("usb_mystic_light.led155_159",    "led155_159", base.SPACE),
    led160_164   	= ProtoField.bytes("usb_mystic_light.led160_164",    "led160_164", base.SPACE),
    led165_169	    = ProtoField.bytes("usb_mystic_light.led165_169",    "led165_169", base.SPACE),
    led170_174	    = ProtoField.bytes("usb_mystic_light.led170_174",    "led170_174", base.SPACE),
    led175_179	    = ProtoField.bytes("usb_mystic_light.led175_179",    "led175_179", base.SPACE),
    led180_184	    = ProtoField.bytes("usb_mystic_light.led180_184",    "led180_184", base.SPACE),
    led185_189	    = ProtoField.bytes("usb_mystic_light.led185_189",    "led185_189", base.SPACE),
    led190_194	    = ProtoField.bytes("usb_mystic_light.led190_194",    "led190_194", base.SPACE),
    led195_199	    = ProtoField.bytes("usb_mystic_light.led195_199",    "led195_199", base.SPACE),
    led200_204	    = ProtoField.bytes("usb_mystic_light.led200_204",    "led200_204", base.SPACE),
    led205_209	    = ProtoField.bytes("usb_mystic_light.led205_209",    "led205_209", base.SPACE),
    led210_214	    = ProtoField.bytes("usb_mystic_light.led210_214",    "led210_214", base.SPACE),
    led215_219	    = ProtoField.bytes("usb_mystic_light.led215_219",    "led215_219", base.SPACE),
    led220_224	    = ProtoField.bytes("usb_mystic_light.led220_224",    "led220_224", base.SPACE),
    led225_229	    = ProtoField.bytes("usb_mystic_light.led225_229",    "led225_229", base.SPACE),
    led230_234	    = ProtoField.bytes("usb_mystic_light.led230_234",    "led230_234", base.SPACE),
    led235_239	    = ProtoField.bytes("usb_mystic_light.led235_239",    "led235_239", base.SPACE),
}

usb_ml_protocol.fields = fields

function usb_ml_protocol.dissector(buffer, pinfo, tree)
	local length = buffer:len()
	-- only accept expected data length
	if (length ~= 192) and (length ~= 185) and (length ~= 169) and (length ~= 162) and (length ~= 732) then return end

	-- detemrmine offset into data for interpretation
	local offset
	if (length == 192) or (length == 169) or (length == 732) then
		offset = 7
	else
		offset = 0
	end

	-- only accept requests 82 and 83 as Mystic Light
	if (tostring(buffer(offset,1)) ~= "52") and (tostring(buffer(offset,1)) ~= "53") then return end

	pinfo.cols.protocol = usb_ml_protocol.name

	if tostring(buffer(offset,1)) == "52" then
		-- handle OEM requests
		if (length == 192) or (length == 185) then
			-- handle 185 byte data
			local subtree = tree:add(usb_ml_protocol, buffer(offset), "USB Mystic Light 185")

			subtree:add(fields.requestType,   buffer(offset,        1))
			subtree:add(fields.jrgb1,         buffer(offset + 1,   10))
			subtree:add(fields.jpipe1,        buffer(offset + 11,  10))
			subtree:add(fields.jpipe2,        buffer(offset + 21,  10))
			subtree:add(fields.jrainbow1,     buffer(offset + 31,  11))
			subtree:add(fields.jrainbow2,     buffer(offset + 42,  11))
			subtree:add(fields.jcorsair,      buffer(offset + 53,  11))
			subtree:add(fields.jcorsairOuter, buffer(offset + 64,  10))
			subtree:add(fields.onboard0,      buffer(offset + 74,  10))
			subtree:add(fields.onboard1,      buffer(offset + 84,  10))
			subtree:add(fields.onboard2,      buffer(offset + 94,  10))
			subtree:add(fields.onboard3,      buffer(offset + 104, 10))
			subtree:add(fields.onboard4,      buffer(offset + 114, 10))
			subtree:add(fields.onboard5,      buffer(offset + 124, 10))
			subtree:add(fields.onboard6,      buffer(offset + 134, 10))
			subtree:add(fields.onboard7,      buffer(offset + 144, 10))
			subtree:add(fields.onboard8,      buffer(offset + 154, 10))
			subtree:add(fields.onboard9,      buffer(offset + 164, 10))
			subtree:add(fields.jrgb2,         buffer(offset + 174, 10))
			subtree:add(fields.save,          buffer(offset + 184,  1))
		elseif (length == 169) or (length == 162) then
			-- handle 162 byte data
			local subtree = tree:add(usb_ml_protocol, buffer(7), "USB Mystic Light 162")

			subtree:add(fields.requestType,   buffer(offset,        1))
			subtree:add(fields.jrgb1,         buffer(offset + 1,   10))
			subtree:add(fields.jrainbow1,     buffer(offset + 11,  10))
			subtree:add(fields.jcorsair,      buffer(offset + 21,  10))
			subtree:add(fields.jcorsairOuter, buffer(offset + 31,  10))
			subtree:add(fields.onboard0,      buffer(offset + 41,  10))
			subtree:add(fields.onboard1,      buffer(offset + 51,  10))
			subtree:add(fields.onboard2,      buffer(offset + 61,  10))
			subtree:add(fields.onboard3,      buffer(offset + 71,  10))
			subtree:add(fields.onboard4,      buffer(offset + 81,  10))
			subtree:add(fields.onboard5,      buffer(offset + 91,  10))
			subtree:add(fields.onboard6,      buffer(offset + 101, 10))
			subtree:add(fields.onboard7,      buffer(offset + 111, 10))
			subtree:add(fields.onboard8,      buffer(offset + 121, 10))
			subtree:add(fields.onboard9,      buffer(offset + 131, 10))
			subtree:add(fields.onboard10,     buffer(offset + 141, 10))
			subtree:add(fields.jrgb2,         buffer(offset + 151, 10))
			subtree:add(fields.save,          buffer(offset + 161,  1))
		end
	else
		-- handle direct LED requests
		local subtree = tree:add(usb_ml_protocol, buffer(7), "USB Mystic Light 185 direct")

		subtree:add(fields.requestType, buffer(offset,        1))
		subtree:add(fields.header,      buffer(offset + 1,    4))
		subtree:add(fields.led0_4,      buffer(offset + 5,   15))
		subtree:add(fields.led5_9,      buffer(offset + 20,  15))
		subtree:add(fields.led10_14,    buffer(offset + 35,  15))
		subtree:add(fields.led15_19,    buffer(offset + 50,  15))
		subtree:add(fields.led20_24,    buffer(offset + 65,  15))
		subtree:add(fields.led25_29,    buffer(offset + 80,  15))
		subtree:add(fields.led30_34,    buffer(offset + 95,  15))
		subtree:add(fields.led35_39,    buffer(offset + 110, 15))
		subtree:add(fields.led40_44,    buffer(offset + 125, 15))
		subtree:add(fields.led45_49,    buffer(offset + 140, 15))
		subtree:add(fields.led50_54,    buffer(offset + 155, 15))
		subtree:add(fields.led55_59,    buffer(offset + 170, 15))
		subtree:add(fields.led60_64,    buffer(offset + 185, 15))
		subtree:add(fields.led65_69,    buffer(offset + 200, 15))
		subtree:add(fields.led70_74,    buffer(offset + 215, 15))
		subtree:add(fields.led75_79,    buffer(offset + 230, 15))
		subtree:add(fields.led80_84,    buffer(offset + 245, 15))
		subtree:add(fields.led85_89,    buffer(offset + 260, 15))
		subtree:add(fields.led90_94,    buffer(offset + 275, 15))
		subtree:add(fields.led95_99,    buffer(offset + 290, 15))
		subtree:add(fields.led100_104,  buffer(offset + 305, 15))
		subtree:add(fields.led105_109,  buffer(offset + 320, 15))
		subtree:add(fields.led110_114,  buffer(offset + 335, 15))
		subtree:add(fields.led115_119,  buffer(offset + 350, 15))
		subtree:add(fields.led120_124,  buffer(offset + 365, 15))
		subtree:add(fields.led125_129,  buffer(offset + 380, 15))
		subtree:add(fields.led130_134,  buffer(offset + 395, 15))
		subtree:add(fields.led135_139,  buffer(offset + 410, 15))
		subtree:add(fields.led140_144,  buffer(offset + 425, 15))
		subtree:add(fields.led145_149,  buffer(offset + 440, 15))
		subtree:add(fields.led150_154,  buffer(offset + 455, 15))
		subtree:add(fields.led155_159,  buffer(offset + 470, 15))
		subtree:add(fields.led160_164,  buffer(offset + 485, 15))
		subtree:add(fields.led165_169,  buffer(offset + 500, 15))
		subtree:add(fields.led170_174,  buffer(offset + 515, 15))
		subtree:add(fields.led175_179,  buffer(offset + 530, 15))
		subtree:add(fields.led180_184,  buffer(offset + 545, 15))
		subtree:add(fields.led185_189,  buffer(offset + 560, 15))
		subtree:add(fields.led190_194,  buffer(offset + 575, 15))
		subtree:add(fields.led195_199,  buffer(offset + 590, 15))
		subtree:add(fields.led200_204,  buffer(offset + 605, 15))
		subtree:add(fields.led205_209,  buffer(offset + 620, 15))
		subtree:add(fields.led210_214,  buffer(offset + 635, 15))
		subtree:add(fields.led215_219,  buffer(offset + 650, 15))
		subtree:add(fields.led220_224,  buffer(offset + 665, 15))
		subtree:add(fields.led225_229,  buffer(offset + 680, 15))
		subtree:add(fields.led230_234,  buffer(offset + 695, 15))
		subtree:add(fields.led235_239,  buffer(offset + 710, 15))
	end
end

DissectorTable.get("usb.control"):add(3, usb_ml_protocol)
DissectorTable.get("usb.control"):add(9, usb_ml_protocol)
DissectorTable.get("usb.control"):add(0xffff, usb_ml_protocol)
